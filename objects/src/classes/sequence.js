import i18n from 'es2015-i18n-tag'
import BaseClass from '../base-class'
import 'reflect-metadata'
/*
r= new Robot()

s = new Séquence()
s.ajouterCommande("r.avancer()")
s.ajouterDélai(800)
s.ajouterCommande("r.avancer()")
s.ajouterDélai(1800)
s.boucler(true)
s.démarrer()
*/

@Reflect.metadata('translated', i18n`Sequence`)
class Sequence extends BaseClass {
  constructor() {
    super()
    this._actions = new Array()
    this._index = -1
    this._MINIMUM_LOOP = 100
    this._running = false
    this._frozen = false
    this._timeout = null
    this._loop = false
    this._wasRunning = false
    this._logCommands = true
  }

  /**
   * Add a command to Sequence.
   * @param {String} command
   */
  @Reflect.metadata('translated', i18n`addCommand`)
  @Reflect.metadata('help', i18n`addCommand_help`)
  addCommand(command) {
    this._actions.push({ type: 0, value: command })
    console.log('command added')
  }
  // 0 Correspond à une commande à exécuter, 1 correspond à un délai

  /**
   * Add a delay between commands.
   * @param {Number} delay    (ms)
   */
  @Reflect.metadata('translated', i18n`addDelay`)
  @Reflect.metadata('help', i18n`addDelay_help`)
  addDelay(delay) {
    this._actions.push({ type: 1, value: delay })
    console.log('delay added')
  }

  /**
   * Execute the next command of Sequence (after waiting if there's a delay).
   */
  nextAction() {
    this._timeout = null
    this._index++
    if (this._actions.length > 0 && this._running) {
      if (this._index >= this._actions.length) {
        if (this._loop) {
          this._index = 0
        } else {
          // last action reached: we stop here
          this._running = false
          return
        }
      }
      var action = this._actions[this._index]
      if (action.type === 0) {
        // execute commands
        this._runtime.executeCode(action.value)
        this.nextAction()
      } else if (action.type === 1) {
        var self = this
        this._timeout = window.setTimeout(function () {
          self.nextAction()
        }, action.value)
      }
    }
  }

  /**
   * Start the execution of the sequence
   */
  @Reflect.metadata('translated', i18n`start`)
  @Reflect.metadata('help', i18n`start_help`)
  start() {
    if (this._running) {
      // Sequence is already running: restart it
      this.stop()
    }
    this._running = true
    this._index = -1
    this.nextAction()
  }

  /**
   * Stop the execution of the sequence
   */
  @Reflect.metadata('translated', i18n`stop`)
  @Reflect.metadata('help', i18n`stop_help`)
  stop() {
    this._running = false
    this._index = -1
    if (this._timeout !== null) {
      window.clearTimeout(this._timeout)
      this._timeout = null
    }
  }

  /**
   * Pause the execution of Sequence. It can resume after.
   */
  @Reflect.metadata('translated', i18n`pause`)
  @Reflect.metadata('help', i18n`pause_help`)
  pause() {
    this._running = false
    if (this._timeout !== null) {
      window.clearTimeout(this._timeout)
      this._timeout = null
    }
  }

  /**
   * Resume the execution of Sequence.
   */
  @Reflect.metadata('translated', i18n`unpause`)
  @Reflect.metadata('help', i18n`unpause_help`)
  unpause() {
    this._running = true
    this.nextAction()
  }

  /**
   * Delete Sequence.
   */
  @Reflect.metadata('translated', i18n`deleteObject`)
  @Reflect.metadata('help', i18n`deleteObject_help`)
  deleteObject() {
    this.stop()
    this.delete()
  }

  /**
   * Enable or disable loops for the execution of Sequence.
   * If it enable it, check the total delay of a loop.
   * If it's under the delay of MINIMUM_LOOP, throw a freeze warning.
   * Default value : false.
   * @param {Boolean} value
   */
  @Reflect.metadata('translated', i18n`loop`)
  @Reflect.metadata('help', i18n`loop_help`)
  loop(value) {
    if (value) {
      // WARNING: in order to prevent Declick from freezing, check that there is at least a total delay of MINIMUM_LOOP in actions
      var totalDelay = 0
      for (var i = 0; i < this._actions.length; i++) {
        var action = this._actions[i]
        if (action.type === 1) {
          totalDelay += action.value
        }
      }
      if (totalDelay < this._MINIMUM_LOOP) {
        throw new Error('freeze warning')
      }
    }
    this._loop = value
  }

  /**
   * Freeze or unfreeze Sequence.
   * @param {Boolean} value
   */
  freeze(value) {
    if (value !== this._frozen) {
      if (value) {
        this._wasRunning = this._running
        this.pause()
        this._frozen = true
      } else {
        if (this._wasRunning) {
          this.unpause()
        }
        this._frozen = false
      }
    }
  }

  /**
   * Enable or disable the display of commands.
   * Default value : true.
   * @param {type} value
   */
  @Reflect.metadata('translated', i18n`displayCommands`)
  @Reflect.metadata('help', i18n`displayCommands_help`)
  displayCommands(value) {
    this._logCommands = value
  }
}

export default Sequence
