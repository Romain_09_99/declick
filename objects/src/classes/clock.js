import i18n from 'es2015-i18n-tag'
import BaseClass from '../base-class'
import 'reflect-metadata'

/*
r= new Robot()

m = new Minuteur()
m.ajouterCommande("r.avancer()")
m.ajouterCommande("r.reculer()")
m.définirDélai(4000)
m.définirDélaiInitial(2000)
m.boucler(true)
m.démarrer()
*/
@Reflect.metadata('translated', i18n`Clock`)
class Clock extends BaseClass {
  constructor() {
    super()
    this._delay = 1000
    this._initialDelay = 0
    this._running = false
    this._wasRunning = false
    this._timeout = null
    this._loop = false
    this._frozen = false
    this._interpreter = null
    this._commands = []
    this._displayCommands = false
  }

  /**
   *Add a command to clock
   *@param {String} command
   */
  @Reflect.metadata('translated', i18n`addCommand`)
  @Reflect.metadata('help', i18n`addCommand_help`)
  addCommand(code) {
    this._commands.push(code)
  }

  /**
   * Remove all commands to Clock.
   */
  @Reflect.metadata('translated', i18n`removeCommands`)
  @Reflect.metadata('help', i18n`removeCommands_help`)
  removeCommands() {
    this._commands = []
  }

  /**
   * Set the initial Delay before the execution of the commands,
   * and after each loop.
   * @param {Number} delay    (ms)
   */
  @Reflect.metadata('translated', i18n`setInitialDelay`)
  @Reflect.metadata('help', i18n`setInitialDelay_help`)
  setInitialDelay(delay) {
    if (!isNaN(delay)) {
      this._initialDelay = delay
    }
  }

  /**
   * Set a Delay between the execution of two commands.
   * If no initial delay is defined, set it to the same value.
   * Default value : 1000 ms.
   * @param {Number} delay    (ms)
   */
  @Reflect.metadata('translated', i18n`setDelay`)
  @Reflect.metadata('help', i18n`setDelay_help`)
  setDelay(delay) {
    this._delay = delay
    if (this._initialDelay === false) {
      this._setInitialDelay(delay)
    }
  }

  /**
   * Start the execution of Clock.
   */
  @Reflect.metadata('translated', i18n`start`)
  @Reflect.metadata('help', i18n`start_help`)
  start() {
    if (!this._running) {
      this._running = true
      var self = this
      this._timeout = window.setTimeout(function () {
        self.executeActions()
      }, this._initialDelay)
    }
  }

  /**
   * Stop the execution of Clock.
   */
  @Reflect.metadata('translated', i18n`stop`)
  @Reflect.metadata('help', i18n`stop_help`)
  stop() {
    this._running = false
    if (this._timeout !== null) {
      window.clearTimeout(this._timeout)
      this._timeout = null
    }
  }

  /**
   * Execute actions linked to Clock.
   */
  executeActions() {
    this._timeout = null
    if (this._running) {
      for (const i in this._commands) {
        this._runtime.executeCode(this._commands[i])
        if (this._displayCommands) {
          alert(`${this._commands[i]}`)
        }
      }
      if (this._loop) {
        var self = this
        this._timeout = window.setTimeout(function () {
          self.executeActions()
        }, this._delay)
      } else {
        this.stop()
      }
    }
  }

  /**
   * Delete Clock.
   */
  @Reflect.metadata('translated', i18n`deleteObject`)
  @Reflect.metadata('help', i18n`deleteObject_help`)
  deleteObject() {
    this.stop()
    this.delete()
  }

  /**
   * Enable or disable loops for the execution of Clock.
   * Default value : true.
   * @param {Boolean} value
   */
  @Reflect.metadata('translated', i18n`loop`)
  @Reflect.metadata('help', i18n`loop_help`)
  loop(value) {
    if (typeof value === 'boolean') {
      this._loop = value
    }
  }

  /**
   *Freeze of unfreeze Clock.
   *@param {Boolean} value
   */
  freeze(value) {
    if (value !== this._frozen) {
      if (value) {
        this._wasRunning = this._running
        this.stop()
        this._frozen = true
      } else {
        if (this._wasRunning) {
          this.start()
        }
        this._frozen = false
      }
    }
  }

  /**
   * Enable or disable the display of commands.
   * Default value : true.
   * @param {type} value
   */
  @Reflect.metadata('translated', i18n`displayCommands`)
  @Reflect.metadata('help', i18n`displayCommands_help`)
  displayCommands(value) {
    if (typeof value === 'boolean') {
      this._displayCommands = value
    }
  }
}

export default Clock
