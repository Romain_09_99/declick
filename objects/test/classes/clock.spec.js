/*eslint-env mocha */
import { assert } from 'chai'
import { i18nConfig } from 'es2015-i18n-tag'
import frenchTranslations from '../../translations/translation.fr.json'
import 'reflect-metadata'

/*
m = new Minuteur()
m.ajouterCommande("a=4")
m.ajouterCommande("a=a+1")
m.démarrer()
*/

describe('When clock is instantiated', () => {

  it('should have an exposed name', () => {
    i18nConfig({
      locales: 'fr-FR',
      translations: frenchTranslations,
    })
    return import('../../src/classes/clock').then(({ default: Clock }) => {
      assert.equal(Reflect.getMetadata('translated', Clock), 'Minuteur')
    })
  })

  it('should have an exposed addCommand method', () => {
    i18nConfig({
      locales: 'fr-FR',
      translations: frenchTranslations,
    })
    return import('../../src/classes/clock').then(({ default: Clock }) => {
      assert.equal(Reflect.getMetadata('translated', Clock.prototype, 'addCommand'), 'ajouterCommande')
      assert.equal(Reflect.getMetadata('help', Clock.prototype, 'addCommand'), 'ajouterCommande()')
    })
  })

  it('should have an exposed remove method', () => {
    i18nConfig({
      locales: 'fr-FR',
      translations: frenchTranslations,
    })
    return import('../../src/classes/Clock').then(({ default: Clock }) => {
      assert.equal(Reflect.getMetadata('translated', Clock.prototype, 'removeCommands'), 'supprimerCommandes')
      assert.equal(Reflect.getMetadata('help', Clock.prototype, 'removeCommands'), 'supprimerCommandes()')
    })
  })

  it('should have an exposed start method', () => {
    i18nConfig({
      locales: 'fr-FR',
      translations: frenchTranslations,
    })
    return import('../../src/classes/Clock').then(({ default: Clock }) => {
      assert.equal(Reflect.getMetadata('translated', Clock.prototype, 'start'), 'démarrer')
      assert.equal(Reflect.getMetadata('help', Clock.prototype, 'start'), 'démarrer()')
    })
  })

  it('should execute all commands', () => {
    return import('../../src/classes/Clock').then(({ default: Clock }) => {
      let min = new Clock()
      min.addCommand("a=1")
      min.addCommand("b=a+1")
      min.addCommand("c=b+1")
      min.start()
      // TODO: how to get "c" value
      // assert.equal(min._runtime.getDeclickObject(min), 3)
      assert.equal(3, 3)
    })
  })

  it('should work delay', () => {
    return import('../../src/classes/Clock').then(({ default: Clock }) => {
      let a = 0
      let t1 = console.time()
      let min = new Clock()
      // min._runtime.executeCode("a=0")
      min.addCommand("a=1; t2 = console.time();t = t2 - t1")
      min.setInitialDelay(2000)
      // min.start()

      // let t2 = console.time()
      // TODO: Besoin d'un listener pour la variable declick a

      // TODO: howto get declick scope ? ( même problème que ligne 66)
      // assert.closeTo(t, 2000, 50)
      assert.equal(true, true)
    })
  })

})
